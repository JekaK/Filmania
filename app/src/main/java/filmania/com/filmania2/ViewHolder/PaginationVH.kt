package filmania.com.filmania2.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
/***
 * Клас - компонент для утримання елементів View і представлення їх в адаптері.
 * Даний клас використовується для пагінції.
 * */
class PaginationVH(itemView: View?) : RecyclerView.ViewHolder(itemView) {

}