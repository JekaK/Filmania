package filmania.com.filmania2.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import filmania.com.filmania2.R
/***
 * Клас - компонент для утримання елементів View і представлення їх в адаптері.
 * Даний клас використовується для класу з фільмами користувача.
 * */
class FilmViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    var detailsButton: Button = itemView!!.findViewById(R.id.details_button)
    var ratingBar: RatingBar = itemView!!.findViewById(R.id.ratingBar_fav)
    var description: TextView = itemView!!.findViewById(R.id.description_fav)
    var title: TextView = itemView!!.findViewById(R.id.movie_title_fav)
    var poster: ImageView = itemView!!.findViewById(R.id.poster_fav)
}