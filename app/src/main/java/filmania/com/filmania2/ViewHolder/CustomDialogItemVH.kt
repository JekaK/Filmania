package filmania.com.filmania2.ViewHolder

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.github.ivbaranov.mli.MaterialLetterIcon
import filmania.com.filmania2.R

class CustomDialogItemVH(item: View?) : RecyclerView.ViewHolder(item) {
    var textDialog = item!!.findViewById<TextView>(R.id.playlist_name_dialog)
    var icon = item!!.findViewById<MaterialLetterIcon>(R.id.icon_playlist_dialog)
    var itemDialog = item!!.findViewById<ConstraintLayout>(R.id.item_dialog)
}