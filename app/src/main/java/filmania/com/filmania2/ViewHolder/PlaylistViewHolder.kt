package filmania.com.filmania2.ViewHolder

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import com.github.ivbaranov.mli.MaterialLetterIcon
import filmania.com.filmania2.R
/***
 * Клас - компонент для утримання елементів View і представлення їх в адаптері.
 * Даний клас використовується для плейлистів користувача.
 * */
class PlaylistViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    var icon = itemView!!.findViewById<MaterialLetterIcon>(R.id.icon_playlist_dialog)
    var playlistText = itemView!!.findViewById<TextView>(R.id.playlist_name)
    var dots = itemView!!.findViewById<ImageButton>(R.id.dots)
    var card = itemView!!.findViewById<CardView>(R.id.card_playlist)
}