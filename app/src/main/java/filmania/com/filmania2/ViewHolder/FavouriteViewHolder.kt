package filmania.com.filmania2.ViewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.*
import filmania.com.filmania2.R

/***
 * Клас - компонент для утримання елементів View і представлення їх в адаптері.
 * Даний клас використовується для улюблених фільмів.
 * */

class FavouriteViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
    var detailsButton: Button = itemView!!.findViewById(R.id.details_button_fav)
    var ratingBar: RatingBar = itemView!!.findViewById(R.id.ratingBar_fav)
    var description: TextView = itemView!!.findViewById(R.id.description_fav)
    var title: TextView = itemView!!.findViewById(R.id.movie_title_fav)
    var poster: ImageView = itemView!!.findViewById(R.id.poster_fav)
    var delete: ImageButton = itemView!!.findViewById(R.id.delete_fav)
}