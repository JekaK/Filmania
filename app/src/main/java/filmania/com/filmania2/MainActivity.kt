package filmania.com.filmania2

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import filmania.com.filmania2.Activity.LogIn
import filmania.com.filmania2.Fragments.NewsFragment
import filmania.com.filmania2.Fragments.ProfileFragment
import filmania.com.filmania2.Fragments.SearchFragment
import filmania.com.filmania2.Util.LocalInfoSave

/**
 * Головна точка входу в додаток. Містить в собі бізнес логіку відображення фрагментів. Також є сховищем для стану додатку.
 * **/

class MainActivity : AppCompatActivity() {
    private var fragments: Array<Fragment> = arrayOf(NewsFragment(), SearchFragment(), ProfileFragment())
    private var selectedFragment: Fragment = fragments[0]
    private var index: Int = -1
    private lateinit var navigation:BottomNavigationView

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.news -> {
                selectedFragment = fragments[0]
                index = 0
            }
            R.id.search -> {
                selectedFragment = fragments[1]
                index = 1
            }
            R.id.profile -> {
                selectedFragment = fragments[2]
                index = 2
            }
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, selectedFragment, fragments[index].javaClass.simpleName)
        transaction.addToBackStack(fragments[index].javaClass.simpleName)
        transaction.commit()
        true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val int: Intent = intent
        if (int.extras != null) {
            println("Values: ${int.extras["values"]}")
        }
        val token = LocalInfoSave(this)

        if (token.getAccessToken() == null) {
            val intent = Intent(this, LogIn::class.java)
            intent.flags = intent.flags or Intent.FLAG_ACTIVITY_NO_HISTORY
            startActivity(intent)
            finish()
        }
        fragments[2].retainInstance = true
        navigation = findViewById(R.id.navigation)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, selectedFragment)
        transaction.commit()
    }
}
