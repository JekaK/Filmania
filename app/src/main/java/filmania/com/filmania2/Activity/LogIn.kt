package filmania.com.filmania2.Activity

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import filmania.com.filmania2.MainActivity
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AuthRequestBody
import filmania.com.filmania2.Request.Body.Response.LoginResponse
import filmania.com.filmania2.Request.Callback.LoginCallback
import filmania.com.filmania2.Request.Request.LoginRequest
import filmania.com.filmania2.Util.LocalInfoSave
import android.view.ViewGroup
import filmania.com.filmania2.Request.Body.Response.RegisterResponse
import filmania.com.filmania2.Request.Callback.RegisterCallback
import filmania.com.filmania2.Request.Request.RegisterRequest
/**
 * Клас для авторизаці в додатку, а такж реєстрації в ньому
 * **/

class LogIn : AppCompatActivity() {
    private lateinit var actionButton: CircularProgressButton
    private lateinit var signIn: TextView
    private lateinit var signUp: TextView
    private lateinit var usernameEdit: EditText
    private lateinit var passEdit: EditText
    private lateinit var inDot: View
    private lateinit var upDot: View

    private var whatAction = true  //true - signIn, false - signUp
    private var focusColor: Int = 0
    private var notFocusColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.log_in)
        initViews()
        initListeners()
    }

    private fun initViews() {
        focusColor = ContextCompat.getColor(this, R.color.OnFocusColor)
        notFocusColor = ContextCompat.getColor(this, R.color.NotOnFocusColor)
        actionButton = findViewById(R.id.action_button)
        signIn = findViewById(R.id.signin_text)
        signUp = findViewById(R.id.signup_text)
        usernameEdit = findViewById(R.id.username_edit)
        passEdit = findViewById(R.id.pass_edit)
        inDot = findViewById(R.id.in_dot)
        upDot = findViewById(R.id.up_dot)
        upDot.visibility = View.INVISIBLE
        signUp.setTextColor(notFocusColor)
    }

    private fun initListeners() {
        signUp.setOnClickListener {
            if (whatAction) {
                signIn.setTextColor(notFocusColor)
                signUp.setTextColor(focusColor)

                inDot.visibility = View.INVISIBLE
                upDot.visibility = View.VISIBLE
                actionButton.text = "SIGN UP"

                whatAction = false
            }
        }
        signIn.setOnClickListener {
            if (!whatAction) {
                signIn.setTextColor(focusColor)
                signUp.setTextColor(notFocusColor)

                inDot.visibility = View.VISIBLE
                upDot.visibility = View.INVISIBLE
                actionButton.text = "SIGN IN"

                whatAction = true
            }
        }
        actionButton.setOnClickListener {
            actionButton.startAnimation()
            val params = actionButton.layoutParams
            params.width = ViewGroup.LayoutParams.WRAP_CONTENT
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT
            actionButton.layoutParams = params

            if (!whatAction) {
                RegisterRequest(this)
                        .registerUser(AuthRequestBody(usernameEdit.text.toString(), passEdit.text.toString()),
                                object : RegisterCallback {
                                    override fun onSuccess(resp: RegisterResponse) {
                                        LocalInfoSave(this@LogIn)
                                                .saveToken(resp.status!!.getToken().toString())
                                        actionButton
                                                .revertAnimation({
                                                    actionButton.text = "SIGN IN"
                                                })
                                        val intent = Intent(this@LogIn, MainActivity::class.java)
                                        intent.putExtra("values", resp)
                                        startActivity(intent)
                                        finish()
                                    }

                                    override fun onFail(t: Throwable) {
                                        actionButton
                                                .revertAnimation({
                                                    actionButton.text = "SIGN IN"
                                                })
                                        Toast.makeText(this@LogIn, t.toString(), Toast.LENGTH_LONG).show()
                                    }
                                })
            } else {

                LoginRequest(this)
                        .loginUser(AuthRequestBody(usernameEdit.text.toString(), passEdit.text.toString()),
                                object : LoginCallback {
                                    override fun onSuccess(resp: LoginResponse) {
                                        LocalInfoSave(this@LogIn)
                                                .saveToken(resp.status!!.key.toString())
                                        actionButton
                                                .revertAnimation({
                                                    actionButton.text = "SIGN UP"
                                                })
                                        val intent = Intent(this@LogIn, MainActivity::class.java)
                                        intent.putExtra("values", resp)
                                        startActivity(intent)
                                        finish()
                                    }

                                    override fun onFail(t: Throwable) {
                                        actionButton
                                                .revertAnimation({
                                                    actionButton.text = "SIGN UP"
                                                })
                                        Toast.makeText(this@LogIn, t.toString(), Toast.LENGTH_LONG).show()
                                    }
                                })
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        actionButton.dispose()
    }
}