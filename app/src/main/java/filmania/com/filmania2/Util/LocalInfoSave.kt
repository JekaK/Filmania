package filmania.com.filmania2.Util

import android.content.Context
import android.content.SharedPreferences
import filmania.com.filmania2.R
/**
 * Клас для зберігання унікального ключа кожного користувача при його авторизації
 * **/
class LocalInfoSave(private var context: Context) {
    private var pref: SharedPreferences = this.context.getSharedPreferences(this.context.resources.getString(R.string.PREF_NAME),
            Integer.parseInt(context.resources.getString(R.string.PRIVATE_MODE)))
    private var editor: SharedPreferences.Editor
    private lateinit var token: String

    public fun getAccessToken(): String? {
        return pref.getString("token", null)
    }


    public fun saveToken(token: String) {
        editor.putString("token", token)
        editor.commit()
    }

    public fun clear() {
        editor.clear()
        editor.commit()
    }

    init {
        editor = pref.edit()
    }
}