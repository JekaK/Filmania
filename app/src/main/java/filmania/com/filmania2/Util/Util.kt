package filmania.com.filmania2.Util

import android.content.Context
import android.net.ConnectivityManager
import android.util.TypedValue

/**
 * Клас помічник для виконнаня функцій перевірки та мапінгу
 * **/

class Util {
    companion object {
        fun isOnline(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }

        fun dpToPxConverter(dp: Int, context: Context): Int {
            return TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dp.toFloat(),
                    context.resources.displayMetrics
            ).toInt()
        }
    }
}