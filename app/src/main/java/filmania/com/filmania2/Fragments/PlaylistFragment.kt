package filmania.com.filmania2.Fragments

import android.app.AlertDialog
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.FavouriteRecyclerAdapter
import filmania.com.filmania2.Adapter.PlaylistAdapter
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.ChangeNameRequestBody
import filmania.com.filmania2.Request.Body.Request.CreatePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.ChangeNameResponseBody
import filmania.com.filmania2.Request.Body.Response.CreatePlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import filmania.com.filmania2.Request.Callback.ChangeNameCallback
import filmania.com.filmania2.Request.Callback.CreatePlaylistCallback
import filmania.com.filmania2.Request.Callback.PlaylistsListCallback
import filmania.com.filmania2.Request.Request.CreatePlaylistRequest
import filmania.com.filmania2.Request.Request.EditPlaylistRequest
import filmania.com.filmania2.Request.Request.PlaylistListRequest
/**
 * Клас - фрагмент який містить в собі бізнес логіку по відображенню списку плейлистів користувача.
 * **/
class PlaylistFragment : Fragment() {
    private lateinit var v: View
    private lateinit var recycler: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: PlaylistAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var fabPlaylist: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.playlist_fragment, container, false)
        initView()
        initListeners()
        return v
    }

    private fun initView() {
        recycler = v.findViewById(R.id.reycler_playlist)
        swipeRefreshLayout = v.findViewById(R.id.swipe_playlist)

        recycler.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = mLayoutManager
        val filmList = PlaylistResponseBody()
        filmList.status = mutableListOf()
        adapter = PlaylistAdapter(activity!!, filmList)
        adapter.setHasStableIds(true)
        recycler.adapter = adapter
        swipeRefreshLayout.isRefreshing = true
        loadData()
        swipeRefreshLayout.setOnRefreshListener {
            loadData()
        }
        fabPlaylist = v.findViewById(R.id.fab_playlist)
    }

    private fun initListeners() {
        fabPlaylist.setOnClickListener({
            val input = EditText(context)
            val lp = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT)
            input.layoutParams = lp
            AlertDialog.Builder(context)
                    .setView(input)
                    .setTitle("Create playlist")
                    .setMessage("Type name of your future playlist")
                    .setPositiveButton("Yes") { dialog, which ->
                        CreatePlaylistRequest(this@PlaylistFragment.context!!)
                                .createPlaylist(CreatePlaylistRequestBody(input.text.toString()),
                                        object : CreatePlaylistCallback {
                                            override fun onSuccess(resp: CreatePlaylistResponseBody) {
                                                swipeRefreshLayout.isRefreshing = true
                                                loadData()
                                            }

                                            override fun onFail(t: Throwable) {
                                                Toasty.error(this@PlaylistFragment.context!!,
                                                        "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                                        .show()
                                            }

                                        })
                    }
                    .setNegativeButton("No") { dialog, which -> }
                    .show()
        })
    }

    private fun loadData() {
        PlaylistListRequest(context!!)
                .getPlaylistList(object : PlaylistsListCallback {
                    override fun onSuccess(resp: PlaylistResponseBody) {
                        adapter.clear()
                        adapter.addAll(resp)
                        swipeRefreshLayout.isRefreshing = false
                    }

                    override fun onFail(t: Throwable) {
                        t.printStackTrace()
                        Toasty.error(this@PlaylistFragment.context!!,
                                "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                .show()
                        swipeRefreshLayout.isRefreshing = false
                    }
                })
    }
}