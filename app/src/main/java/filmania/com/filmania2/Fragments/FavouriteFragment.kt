package filmania.com.filmania2.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.FavouriteRecyclerAdapter
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import filmania.com.filmania2.Request.Callback.FavouriteCallback
import filmania.com.filmania2.Request.Request.FavouriteRequest

/**
 * Клас - фрагмент в якому містятиться бізнес логіка по відображенню інформації що до улюблених фільмів,
 * доданих у список користувачем раніше
 *
 * **/

class FavouriteFragment : Fragment() {
    private lateinit var v: View
    private lateinit var recycler: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: FavouriteRecyclerAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.favourite_fragment, container, false)
        initView()
        return v
    }

    private fun initView() {
        recycler = v.findViewById(R.id.recycler_favourite)
        swipeRefreshLayout = v.findViewById(R.id.swipe_fav)

        recycler.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = mLayoutManager
        val filmList = FavouriteListResponse()
        filmList.resultNews = mutableListOf()
        adapter = FavouriteRecyclerAdapter(activity!!, filmList)
        adapter.setHasStableIds(true)
        recycler.adapter = adapter
        swipeRefreshLayout.isRefreshing = true
        loadData()
        swipeRefreshLayout.setOnRefreshListener {
            loadData()
        }

    }

    private fun loadData() {
        FavouriteRequest(activity!!)
                .getFavourite(object : FavouriteCallback {
                    override fun onSuccess(resp: FavouriteListResponse) {
                        adapter.clear()
                        adapter.addAll(resp)
                        swipeRefreshLayout.isRefreshing = false
                    }

                    override fun onFail(t: Throwable) {
                        t.printStackTrace()
                        Toasty.error(this@FavouriteFragment.context!!,
                                "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                .show()
                        swipeRefreshLayout.isRefreshing = false
                    }
                })
    }
}