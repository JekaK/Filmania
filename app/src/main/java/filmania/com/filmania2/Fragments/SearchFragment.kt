package filmania.com.filmania2.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.Toast
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.NewsSearchRecyclerAdapter
import filmania.com.filmania2.Listener.PaginationLinearScrollListener
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import filmania.com.filmania2.Request.Callback.NewSearchFilmsCallback
import filmania.com.filmania2.Request.Request.SearchRequest
import filmania.com.filmania2.Util.Util

/**
 * Клас - фрагмент, який містить в собі бізнес - логіку відображення списку пошуку та його подальшу обробку.
 * **/

class SearchFragment : Fragment(), MenuItemCompat.OnActionExpandListener {

    private lateinit var v: View
    private lateinit var recycler: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: NewsSearchRecyclerAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var search: Toolbar
    private lateinit var searchMenuItem: MenuItem
    private val PAGE_START = 1
    private var isLoading = false
    private var isLastPage = false
    private var TOTAL_PAGES = 1
    private var currentPage = PAGE_START
    private var query: String? = null
    private var resp: NewSearchFilmListResponse? = null

    enum class Mode {
        NORMAL, SEARCH
    }

    var mode: Mode? = null
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("results", resp)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.fragment_search, container, false)
        search = v.findViewById(R.id.include) as Toolbar
        (activity as AppCompatActivity).setSupportActionBar(search)
        setHasOptionsMenu(true)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        if (savedInstanceState != null) {
            resp = savedInstanceState.getSerializable("results") as NewSearchFilmListResponse
        }
        mode = Mode.NORMAL;
        initView()
        return v
    }

    private fun initFakeList(): NewSearchFilmListResponse {
        if (resp == null) {
            val filmList = NewSearchFilmListResponse()
            filmList.results = mutableListOf()
            filmList.totalPages = 0
            filmList.totalResults = 0
            return filmList
        } else {
            return resp as NewSearchFilmListResponse
        }
    }

    private fun initView() {
        recycler = v.findViewById(R.id.search_recycler)
        recycler.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = mLayoutManager
        adapter = NewsSearchRecyclerAdapter(activity!!, initFakeList())
        adapter.setHasStableIds(true)
        recycler.adapter = adapter

        recycler.addOnScrollListener(object : PaginationLinearScrollListener(context,
                mLayoutManager) {
            override fun loadMoreItems() {
                if (Util.isOnline(this@SearchFragment.context!!)) {
                    if (!adapter.isConnected()) {
                        adapter.setIsConnected(true)
                        adapter.addLoadingFooter()
                    }
                    this@SearchFragment.isLoading = true
                    currentPage += 1
                    loadNextPage(this@SearchFragment.query!!, currentPage)
                } else {
                    if (adapter.isConnected()) {
                        adapter.setIsConnected(false)
                        adapter.removeLoadingFooter()
                    } else {
                        adapter.setIsConnected(false)
                    }
                }
            }

            override fun getTotalPageCount(): Int {
                return this@SearchFragment.TOTAL_PAGES
            }

            override fun isLastPage(): Boolean {
                return this@SearchFragment.isLastPage
            }

            override fun isLoading(): Boolean {
                return this@SearchFragment.isLoading
            }
        })

        swipeRefreshLayout = v.findViewById(R.id.swipe_search)
        swipeRefreshLayout.setOnRefreshListener {
            if (query != null)
                loadFirstPage(query!!)
            else
                swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun loadFirstPage(query: String) {
        if (Util.isOnline(this.context!!)) {
            SearchRequest(this.context!!)
                    .search(query, 1, object : NewSearchFilmsCallback {
                        override fun onSuccess(resp: NewSearchFilmListResponse) {
                            if (resp.totalPages != 0) {
                                TOTAL_PAGES = resp.totalPages!!
                                currentPage = 1
                                isLastPage = false
                                this@SearchFragment.resp = resp
                                adapter.clear()
                                adapter.addAll(resp.results!!)
                                if (recycler.adapter == null)
                                    recycler.adapter = adapter
                                adapter.setIsConnected(true)
                                if (currentPage != TOTAL_PAGES)
                                    adapter.addLoadingFooter()
                                else
                                    isLastPage = true
                                swipeRefreshLayout.isRefreshing = false

                            } else {
                                this@SearchFragment.resp = resp
                                isLastPage = true
                                swipeRefreshLayout.isRefreshing = false

                            }
                        }

                        override fun onFail(t: Throwable) {
                            swipeRefreshLayout.isRefreshing = false

                        }
                    })
        } else {
            adapter.setIsConnected(false)
            Toasty.error(this@SearchFragment.context!!,
                    resources.getString(R.string.NO_INTERNET),
                    Toast.LENGTH_SHORT, true).show()
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun loadNextPage(query: String, page: Int) {
        if (Util.isOnline(this.context!!)) {
            SearchRequest(context!!)
                    .search(query,
                            page,
                            object : NewSearchFilmsCallback {
                                override fun onSuccess(resp: NewSearchFilmListResponse) {
                                    TOTAL_PAGES = resp.totalPages!!
                                    this@SearchFragment.resp!!.results!!.addAll(resp.results!!)
                                    adapter.removeLoadingFooter()
                                    isLoading = false
                                    adapter.addAll(resp.results!!)
                                    if (currentPage != TOTAL_PAGES)
                                        adapter.addLoadingFooter()
                                    else
                                        isLastPage = true
                                }

                                override fun onFail(t: Throwable) {
                                }
                            })
        } else {
            Toasty.error(context!!, resources.getString(R.string.NO_INTERNET), Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.search, menu)
        searchMenuItem = menu!!.findItem(R.id.action_search)
        val sv = searchMenuItem.actionView as SearchView
        sv.maxWidth = Integer.MAX_VALUE

        sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query !== null) {
                    this@SearchFragment.query = query
                    swipeRefreshLayout.isRefreshing = true
                    loadFirstPage(query)
                }
                return true
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        mode = Mode.SEARCH
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        mode = Mode.NORMAL
        return true
    }
}