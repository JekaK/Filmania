package filmania.com.filmania2.Fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.UserProfileResponseBody
import filmania.com.filmania2.Request.Callback.UserInfoCallback
import filmania.com.filmania2.Request.Request.UserInfoRequest

/**
 * Клас - фрагмент, який містить в собі бізнес - логіку відображення профілію користувача,
 * а також всієї інформації що стосується нього.
 * **/
class ProfileFragment : Fragment() {

    private lateinit var v: View
    private lateinit var name: TextView
    private lateinit var email: TextView
    private lateinit var favouriteCount: TextView
    private lateinit var playlistCount: TextView
    private lateinit var avatar: CircleImageView

    private var resp: UserProfileResponseBody? = null

    private lateinit var c: FragmentActivity

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        c = context!! as FragmentActivity
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            resp = savedInstanceState.getSerializable("results") as UserProfileResponseBody
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("results", resp)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.profile_fragment, container, false)
        initView()
        createUserInfoRequest()
        initListeners()
        return v
    }

    private fun initView() {
        name = v.findViewById(R.id.name)
        email = v.findViewById(R.id.email)
        favouriteCount = v.findViewById(R.id.f_c)
        playlistCount = v.findViewById(R.id.p_c)
        avatar = v.findViewById(R.id.avatar)

    }

    private fun initListeners() {
        favouriteCount.setOnClickListener {
            val selectedFragment: android.support.v4.app.Fragment = FavouriteFragment()
            c.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, selectedFragment, selectedFragment::class.java.simpleName)
                    .addToBackStack(c.supportFragmentManager.findFragmentById(R.id.frame_container).javaClass.simpleName)
                    .commit()
        }
        playlistCount.setOnClickListener({
            val selectedFragment: android.support.v4.app.Fragment = PlaylistFragment()
            c.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, selectedFragment, selectedFragment::class.java.simpleName)
                    .addToBackStack(c.supportFragmentManager.findFragmentById(R.id.frame_container).javaClass.simpleName)
                    .commit()
        })
    }

    private fun createUserInfoRequest() {
        if (resp === null)
            UserInfoRequest(c)
                    .getUserInfo(object : UserInfoCallback {
                        override fun onSuccess(resp: UserProfileResponseBody) {
                            if (name.text !== resp.status!!.username)
                                name.text = resp.status!!.username
                            if (resp.status!!.email !== null) {
                                if (email.text != resp.status!!.email)
                                    email.text = resp.status!!.email
                                email.visibility = View.VISIBLE
                            }
                            if (favouriteCount.text !== resp.status!!.favouriteC.toString())
                                favouriteCount.text = resp.status!!.favouriteC.toString()
                            if (playlistCount.text !== resp.status!!.playlistC.toString())
                                playlistCount.text = resp.status!!.playlistC.toString()
                            if (resp.status!!.photo !== null) {
                                val requestOptions = RequestOptions()
                                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                                Glide
                                        .with(c)
                                        .load(c.getString(R.string.BASE_URL) + resp.status!!.photo)
                                        .apply(requestOptions)
                                        .into(avatar)
                            }
                        }

                        override fun onFail(t: Throwable) {
                        }
                    })
        else {
            name.text = resp!!.status!!.username
            if (resp!!.status!!.email !== null) {
                email.text = resp!!.status!!.email
                email.visibility = View.VISIBLE
            }
            favouriteCount.text = resp!!.status!!.favouriteC.toString()
            playlistCount.text = resp!!.status!!.playlistC.toString()
            if (resp!!.status!!.photo !== null) {
                val requestOptions = RequestOptions()
                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                Glide
                        .with(c)
                        .load(c.getString(R.string.BASE_URL) + resp!!.status!!.photo)
                        .apply(requestOptions)
                        .into(avatar)
            }
        }
    }
}
