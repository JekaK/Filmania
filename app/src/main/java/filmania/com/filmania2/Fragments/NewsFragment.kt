package filmania.com.filmania2.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.airbnb.lottie.LottieAnimationView
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.NewsSearchRecyclerAdapter
import filmania.com.filmania2.Listener.PaginationLinearScrollListener
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import filmania.com.filmania2.Request.Callback.NewSearchFilmsCallback
import filmania.com.filmania2.Request.Request.NewFilmRequest
import filmania.com.filmania2.Util.LocalInfoSave
import filmania.com.filmania2.Util.Util
/**
 * Клас - фрагмент в якому міститься бізнес - логіка по відображенню списку нових фільмів.
 *
 * **/
class NewsFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var v: View
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: NewsSearchRecyclerAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var emptyText: TextView
    private lateinit var lotie: LottieAnimationView

    private val PAGE_START = 1
    private var isLoading = false
    private var isLastPage = false
    private var TOTAL_PAGES = 1
    private var currentPage = PAGE_START
    private var resp: NewSearchFilmListResponse? = null

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable("results", resp)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.news_fragment, container, false)
        if (savedInstanceState != null) {
            resp = savedInstanceState.getSerializable("results") as NewSearchFilmListResponse
        }
        initViews()

        return v
    }

    private fun initFakeList(): NewSearchFilmListResponse {
        if (resp == null) {
            val filmList = NewSearchFilmListResponse()
            filmList.results = mutableListOf()
            filmList.totalPages = 0
            filmList.totalResults = 0
            return filmList
        } else {
            return resp as NewSearchFilmListResponse
        }
    }

    private fun initViews() {
        recyclerView = v.findViewById(R.id.news_list)
        recyclerView.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = mLayoutManager
        adapter = NewsSearchRecyclerAdapter(activity!!, initFakeList())
        adapter.setHasStableIds(true)
        recyclerView.adapter = adapter

        recyclerView.addOnScrollListener(object : PaginationLinearScrollListener(context,
                mLayoutManager) {
            override fun loadMoreItems() {
                if (Util.isOnline(this@NewsFragment.context!!)) {
                    if (!adapter.isConnected()) {
                        adapter.setIsConnected(true)
                        adapter.addLoadingFooter()
                    }
                    this@NewsFragment.isLoading = true
                    currentPage += 1
                    loadNextPage(currentPage)
                } else {
                    if (adapter.isConnected()) {
                        adapter.setIsConnected(false)
                        adapter.removeLoadingFooter()
                    } else {
                        adapter.setIsConnected(false)
                    }
                }
            }

            override fun getTotalPageCount(): Int {
                return this@NewsFragment.TOTAL_PAGES
            }

            override fun isLastPage(): Boolean {
                return this@NewsFragment.isLastPage
            }

            override fun isLoading(): Boolean {
                return this@NewsFragment.isLoading
            }
        })
        emptyText = v.findViewById(R.id.empty_text)
        lotie = v.findViewById(R.id.empty_item)
        swipeRefreshLayout = v.findViewById(R.id.swipe_news)
        swipeRefreshLayout.setOnRefreshListener {
            loadFirstPage()
        }
        if (this@NewsFragment.resp == null) {
            swipeRefreshLayout.isRefreshing = true
            loadFirstPage()
        }
    }

    private fun loadFirstPage() {
        if (Util.isOnline(this.context!!)) {
            NewFilmRequest(this.context!!)
                    .getNewFilms(LocalInfoSave(this.context!!).getAccessToken()!!, 1, object : NewSearchFilmsCallback {
                        override fun onSuccess(resp: NewSearchFilmListResponse) {
                            if (resp.totalPages != 0) {
                                TOTAL_PAGES = resp.totalPages!!
                                currentPage = 1
                                isLastPage = false
                                this@NewsFragment.resp = resp
                                adapter.clear()
                                adapter.addAll(resp.results!!)
                                if (recyclerView.adapter == null)
                                    recyclerView.adapter = adapter
                                adapter.setIsConnected(true)
                                if (currentPage != TOTAL_PAGES)
                                    adapter.addLoadingFooter()
                                else
                                    isLastPage = true
                                swipeRefreshLayout.isRefreshing = false
                                lotie.visibility = View.INVISIBLE
                                emptyText.visibility = View.INVISIBLE
                            } else {
                                this@NewsFragment.resp = resp
                                isLastPage = true
                                swipeRefreshLayout.isRefreshing = false
                                lotie.visibility = View.INVISIBLE
                                emptyText.visibility = View.INVISIBLE
                            }
                        }

                        override fun onFail(t: Throwable) {
                            swipeRefreshLayout.isRefreshing = false

                        }
                    })
        } else {
            adapter.setIsConnected(false)
            Toasty.error(this@NewsFragment.context!!,
                    resources.getString(R.string.NO_INTERNET),
                    Toast.LENGTH_SHORT, true).show()
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun loadNextPage(page: Int) {
        if (Util.isOnline(this.context!!)) {
            NewFilmRequest(context!!)
                    .getNewFilms(LocalInfoSave(this.context!!).getAccessToken()!!,
                            page,
                            object : NewSearchFilmsCallback {
                                override fun onSuccess(resp: NewSearchFilmListResponse) {
                                    TOTAL_PAGES = resp.totalPages!!
                                    this@NewsFragment.resp!!.results!!.addAll(resp.results!!)
                                    adapter.removeLoadingFooter()
                                    isLoading = false
                                    adapter.addAll(resp.results!!)
                                    if (currentPage != TOTAL_PAGES)
                                        adapter.addLoadingFooter()
                                    else
                                        isLastPage = true
                                }

                                override fun onFail(t: Throwable) {
                                }
                            })
        } else {
            Toasty.error(context!!, resources.getString(R.string.NO_INTERNET), Toast.LENGTH_SHORT, true).show()
        }
    }

    override fun onResume() {
        super.onResume()
        lotie.visibility = View.INVISIBLE
        emptyText.visibility = View.INVISIBLE
    }
}