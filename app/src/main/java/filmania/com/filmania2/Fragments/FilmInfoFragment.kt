package filmania.com.filmania2.Fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.CustomDialogListViewAdapter
import filmania.com.filmania2.CutomView.MaterialBadgeTextView
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AdditionalInfoRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieAdditionalInfo
import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.StatusPlaylistResponse
import filmania.com.filmania2.Request.Callback.AdditionalInfoCallback
import filmania.com.filmania2.Request.Callback.DefaultCallback
import filmania.com.filmania2.Request.Callback.PlaylistsListCallback
import filmania.com.filmania2.Request.Request.AdditionalInfoRequest
import filmania.com.filmania2.Request.Request.PlaylistListRequest
import filmania.com.filmania2.Util.Util


/**
 * Клас - фрагмент в якому містятиться бізнес логіка по відображенню інформації про фільм.
 *
 * **/
class FilmInfoFragment : Fragment() {
    private lateinit var v: View
    private lateinit var name: TextView
    private lateinit var tagline: TextView
    private lateinit var badges: LinearLayout
    private lateinit var progressBar: ProgressBar
    private lateinit var imageviewInfo: ImageView
    private lateinit var descriptionText: TextView
    private lateinit var budget: TextView
    private lateinit var languageBadges: LinearLayout
    private lateinit var appBar: AppBarLayout
    private lateinit var fab: FloatingActionButton

    private var id: String? = null
    private lateinit var c: Context

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        c = context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.movie_profile, container, false)
        id = arguments!!.getString("id")

        initViews()
        initInfo(id)
        return v
    }

    private fun initViews() {
        name = v.findViewById(R.id.film_name)
        tagline = v.findViewById(R.id.tagline)
        badges = v.findViewById(R.id.badges)
        progressBar = v.findViewById(R.id.progress_movie_info)
        imageviewInfo = v.findViewById(R.id.imageViewInfo)
        descriptionText = v.findViewById(R.id.descriptionText)
        budget = v.findViewById(R.id.budget)
        languageBadges = v.findViewById(R.id.languageBadges)
        appBar = v.findViewById(R.id.app_bar)
        fab = v.findViewById(R.id.fab_fav)

        appBar.addOnOffsetChangedListener({ appBarLayout, verticalOffset ->
            run {
                Log.d("tag_scroll", "recycler_view current offset: " + verticalOffset);
                if (verticalOffset < -100) {
                    if (fab.isShown)
                        fab.hide()
                }
                if (verticalOffset >= -100) {
                    if (!fab.isShown) {
                        fab.show()
                    }
                }
            }
        })

    }

    private fun initInfo(id: String?) {
        AdditionalInfoRequest(this.activity!!)
                .getAdditionalInfo(AdditionalInfoRequestBody(id!!), object : AdditionalInfoCallback {
                    override fun onSuccess(resp: MovieAdditionalInfo) {
                        name.text = resp.result!!.title
                        tagline.text = "\"" + resp.result!!.tagline + "\""
                        val requestOptions = RequestOptions()
                        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                        Glide
                                .with(c)
                                .load(c.getString(R.string.IMAGE_BASE_URL) + resp.result!!.posterPath)
                                .apply(requestOptions)
                                .into(imageviewInfo)
                        val genreBadges = arrayOfNulls<View>(resp.result!!.genres!!.size)
                        badges.removeAllViews()

                        for (i in genreBadges.indices) {

                            val divider = ImageView(c)
                            val lp2 = LinearLayout.LayoutParams(Util.dpToPxConverter(3,
                                    c), 5)
                            divider.layoutParams = lp2

                            val badgeText: TextView = MaterialBadgeTextView(c)
                            badgeText.text = resp.result!!.genres!![i].name

                            badges.addView(badgeText)
                            badges.addView(divider)
                        }

                        descriptionText.text = resp.result!!.overview
                        budget.text = resp.result!!.budget.toString() + "$"

                        val lanBadges = arrayOfNulls<View>(resp.result!!.spokenLanguages!!.size)
                        languageBadges.removeAllViews()

                        for (i in lanBadges.indices) {

                            val divider = ImageView(c)
                            val lp2 = LinearLayout.LayoutParams(Util.dpToPxConverter(3,
                                    c), 5)
                            divider.layoutParams = lp2

                            val badgeText: TextView = MaterialBadgeTextView(c)
                            badgeText.text = resp.result!!.spokenLanguages!![i].iso6391

                            languageBadges.addView(badgeText)
                            languageBadges.addView(divider)
                        }
                        progressBar.visibility = View.INVISIBLE
                    }

                    override fun onFail(t: Throwable) {
                        progressBar.visibility = View.INVISIBLE
                        Toasty.error(c, "Something wrong", Toast.LENGTH_LONG).show()
                    }
                })
        fab.setOnClickListener({

            val alertDialog = AlertDialog.Builder(this@FilmInfoFragment.context)
            val inflater = layoutInflater
            val convertView = inflater.inflate(R.layout.custom_dialog, null) as View
            alertDialog.setView(convertView)
            alertDialog.setTitle("List")
            val lv = convertView.findViewById(R.id.choise_list) as ListView
            var dialog: AlertDialog? = null
            PlaylistListRequest(context!!)
                    .getPlaylistList(object : PlaylistsListCallback {
                        override fun onSuccess(resp: PlaylistResponseBody) {
                            resp.status!!.add(0, StatusPlaylistResponse("Favourite"))
                            val adapter = CustomDialogListViewAdapter(context!!, resp.status!!, id,
                                    object : DefaultCallback {
                                        override fun onSuccess() {
                                            dialog!!.dismiss()
                                            progressBar.visibility = View.INVISIBLE
                                        }

                                        override fun onFail() {
                                            progressBar.visibility = View.INVISIBLE
                                        }
                                    })
                            lv.adapter = adapter
                        }

                        override fun onFail(t: Throwable) {
                            t.printStackTrace()
                            Toasty.error(this@FilmInfoFragment.context!!,
                                    "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                    .show()
                            dialog!!.dismiss()
                        }
                    })

            dialog = alertDialog.create()
            dialog!!.show()
        })
    }
}