package filmania.com.filmania2.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Adapter.MovieFromPlaylistAdapter
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.MovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieFromPlaylistResponse
import filmania.com.filmania2.Request.Callback.MovieFromPlaylistCallback
import filmania.com.filmania2.Request.Request.MoviesFromPlaylistRequest

/**
 * Клас - фрагмент, який містить бізнес логіку відображення списку фільмів в певному плейлисті.
 * **/

class PlaylistMovieListFragment : Fragment() {
    private lateinit var v: View
    private lateinit var recycler: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var adapter: MovieFromPlaylistAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var idList: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        v = inflater.inflate(R.layout.playlist_movie_list_fragment, container, false)
        idList = arguments!!.getInt("id")
        initView()
        return v
    }

    private fun initView() {
        recycler = v.findViewById(R.id.recycler_playlist_movie)
        swipeRefreshLayout = v.findViewById(R.id.swipe_playlist_movie)
        recycler.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = mLayoutManager
        val filmList = MovieFromPlaylistResponse()
        filmList.status = mutableListOf()
        adapter = MovieFromPlaylistAdapter(activity!!, filmList)
        adapter.setHasStableIds(true)
        recycler.adapter = adapter
        loadData()
        swipeRefreshLayout.setOnRefreshListener {
            loadData()
        }
    }

    private fun loadData() {
        MoviesFromPlaylistRequest(this@PlaylistMovieListFragment.context!!)
                .getMoviesFromPlaylist(MovieFromPlaylistRequestBody(idList),
                        object : MovieFromPlaylistCallback {
                            override fun onSuccess(resp: MovieFromPlaylistResponse) {
                                adapter.clear()
                                adapter.addAll(resp)
                                swipeRefreshLayout.isRefreshing = false
                            }

                            override fun onFail(t: Throwable) {
                                t.printStackTrace()
                                Toasty.error(this@PlaylistMovieListFragment.context!!,
                                        "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                        .show()
                                swipeRefreshLayout.isRefreshing = false
                            }
                        })
    }
}