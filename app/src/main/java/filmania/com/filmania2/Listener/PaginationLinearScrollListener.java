package filmania.com.filmania2.Listener;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

/**
 * Абстрактний класс - помічник для використання функції класу - слухача при прокрутці списку і додавання в нього логіки пагінації
 * **/

public abstract class PaginationLinearScrollListener extends RecyclerView.OnScrollListener {
    private LinearLayoutManager layoutManager;
    private Context context;

    public PaginationLinearScrollListener(Context context, LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        this.context = context;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
        if (!isLoading() && !isLastPage()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                    && firstVisibleItemPosition >= 0) {
                loadMoreItems();
            }
        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        final RequestManager picasso = Glide.with(context);
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            picasso.resumeRequests();
        } else {
            picasso.pauseRequests();
        }
    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}