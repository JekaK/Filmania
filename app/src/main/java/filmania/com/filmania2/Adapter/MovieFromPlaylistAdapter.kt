package filmania.com.filmania2.Adapter

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Fragments.FilmInfoFragment
import filmania.com.filmania2.Fragments.PlaylistMovieListFragment
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.DeleteFromFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Request.DeleteMovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Request.DeletePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.*
import filmania.com.filmania2.Request.Callback.DeleteFromFavouriteCallback
import filmania.com.filmania2.Request.Callback.DeleteMovieFromPlaylistCallback
import filmania.com.filmania2.Request.Request.DeleteFromFavouriteRequest
import filmania.com.filmania2.Request.Request.DeleteMovieFromPlaylistRequest
import filmania.com.filmania2.ViewHolder.FavouriteViewHolder
import okhttp3.ResponseBody

/**
 * Клас - адаптер для відображення списку фільмів в плейлисті.
 * Клас включає в себе логіку відображення списку фільмів,
 * а також логіку дій при натисканні кнопок.
 * **/

class MovieFromPlaylistAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var resFav: MovieFromPlaylistResponse
    private lateinit var context: FragmentActivity

    constructor(context: FragmentActivity, resFav: MovieFromPlaylistResponse) : this() {
        this.context = context
        this.resFav = resFav
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: FavouriteViewHolder
        val inflater = LayoutInflater.from(parent.context)
        viewHolder = FavouriteViewHolder(inflater.inflate(R.layout.favourite_card, parent, false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return resFav.status!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val filmRes = resFav.status!![position]
        val movieHolder = holder as FavouriteViewHolder
        movieHolder.description.text = filmRes.movieInfo!!.overview
        movieHolder.title.text = filmRes.movieInfo!!.originalTitle

        val requestOptions = RequestOptions()
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide
                .with(context)
                .load(context.getString(R.string.IMAGE_BASE_URL) + filmRes.movieInfo!!.posterPath)
                .apply(requestOptions)
                .into(movieHolder.poster)
        movieHolder.ratingBar.rating = (filmRes.movieInfo!!.voteAverage!!.toFloat() * 5) / 10
        var selectedFragment: android.support.v4.app.Fragment? = null

        movieHolder.detailsButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id", filmRes.movieId)
            selectedFragment = FilmInfoFragment()
            (selectedFragment as FilmInfoFragment).arguments = bundle

            context.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, selectedFragment, selectedFragment!!::class.java.simpleName)
                    .addToBackStack(context.supportFragmentManager.findFragmentById(R.id.frame_container).javaClass.simpleName)
                    .commit()
        }
        movieHolder.delete.setOnClickListener({
            DeleteMovieFromPlaylistRequest(context)
                    .deleteMovieFromPlaylist(DeleteMovieFromPlaylistRequestBody(filmRes.movieId!!.toInt(),
                            filmRes.playlistId!!),
                            object : DeleteMovieFromPlaylistCallback {
                                override fun onSuccess(resp: DeleteMovieFromPlaylistResponseBody) {
                                    resFav.status!!.removeAt(position)
                                    if (position != 0)
                                        notifyItemRemoved(position)
                                    else
                                        notifyDataSetChanged()
                                }

                                override fun onFail(t: Throwable) {
                                    Toasty.error(context, "Oops, something wrong.")
                                }

                            })
        })
    }

    fun clear() {
        val size = resFav.status!!.size
        if (size > 0) {
            for (i in 0 until size) {
                resFav.status!!.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun add(fav: StatusMoviesFromPlaylist) {
        resFav.status!!.add(fav)
        notifyItemInserted(resFav.status!!.size - 1)
        notifyItemChanged(resFav.status!!.size - 1)
    }

    fun addAll(list: MovieFromPlaylistResponse) {
        for (q in list.status!!) {
            add(q)
        }
    }
}