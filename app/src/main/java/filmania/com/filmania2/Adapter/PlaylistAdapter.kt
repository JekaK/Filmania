package filmania.com.filmania2.Adapter

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Fragments.FilmInfoFragment
import filmania.com.filmania2.Fragments.PlaylistMovieListFragment
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.ChangeNameRequestBody
import filmania.com.filmania2.Request.Body.Request.DeletePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.ChangeNameResponseBody
import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.StatusPlaylistResponse
import filmania.com.filmania2.Request.Callback.ChangeNameCallback
import filmania.com.filmania2.Request.Callback.DeleteCallback
import filmania.com.filmania2.Request.Request.DeleteRequest
import filmania.com.filmania2.Request.Request.EditPlaylistRequest
import filmania.com.filmania2.ViewHolder.PlaylistViewHolder

/**
 * Клас - адаптер для відображення списку Плейлистів. Клас включає в себе логіку відображення списку фільмів,
 * а також логіку дій при натисканні кнопок.
 * **/

class PlaylistAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var list: PlaylistResponseBody
    private lateinit var context: FragmentActivity

    constructor(context: FragmentActivity, list: PlaylistResponseBody) : this() {
        this.list = list
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: PlaylistViewHolder
        val inflater = LayoutInflater.from(parent.context)
        viewHolder = PlaylistViewHolder(inflater.inflate(R.layout.playlist_card, parent, false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.status!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val filmRes = list.status!![position]
        val playlistHolder = holder as PlaylistViewHolder
        playlistHolder.icon.lettersNumber = 1
        playlistHolder.icon.letter = filmRes.name!!.get(0).toString()
        playlistHolder.playlistText.text = filmRes.name
        playlistHolder.dots.setOnClickListener({
            val popup = PopupMenu(context, holder.dots)
            popup.inflate(R.menu.options_menu)
            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.edit -> {
                        val input = EditText(context)
                        val lp = LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.MATCH_PARENT)
                        input.layoutParams = lp
                        AlertDialog.Builder(context)
                                .setView(input)
                                .setTitle("EditPlaylist")
                                .setPositiveButton("Yes") { dialog, which ->
                                    EditPlaylistRequest(context)
                                            .changeName(ChangeNameRequestBody(filmRes.id!!, input.text.toString()),
                                                    object : ChangeNameCallback {
                                                        override fun onSuccess(resp: ChangeNameResponseBody) {
                                                            list.status!![position].name = input.text.toString()
                                                            println(list.status!![position].name)
                                                            notifyDataSetChanged()
                                                        }

                                                        override fun onFail(t: Throwable) {
                                                            Toasty.error(context,
                                                                    "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                                                    .show()
                                                        }

                                                    })
                                }
                                .setNegativeButton("No") { dialog, which -> }
                                .show()

                        true
                    }
                    R.id.delete -> {
                        AlertDialog.Builder(context)
                                .setTitle("Delete playlist?")
                                .setPositiveButton("Yes") { dialog, which ->
                                    DeleteRequest(context)
                                            .deletePlaylist(DeletePlaylistRequestBody(filmRes.id!!),
                                                    object : DeleteCallback {
                                                        override fun onFail(t: Throwable) {
                                                            Toasty.error(context,
                                                                    "Oops, some error occurred.", Toast.LENGTH_SHORT)
                                                                    .show()
                                                        }

                                                        override fun onSuccess(resp: DeleteMovieFromPlaylistResponseBody) {
                                                            list.status!!.removeAt(position)
                                                            notifyItemRemoved(position)
                                                            notifyDataSetChanged()
                                                        }
                                                    })
                                }
                                .setNegativeButton("No") { dialog, which -> }
                                .show()
                        true
                    }


                    else -> false
                }
            }
            popup.show()
        })
        playlistHolder.card.setOnClickListener({
            var selectedFragment: android.support.v4.app.Fragment? = null

            val bundle = Bundle()
            bundle.putInt("id", list.status!![position].id!!)
            selectedFragment = PlaylistMovieListFragment()
            selectedFragment.arguments = bundle

            context.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, selectedFragment, selectedFragment::class.java.simpleName)
                    .addToBackStack(null)
                    .commit()
        })
    }

    fun clear() {
        val size = list.status!!.size
        if (size > 0) {
            for (i in 0 until size) {
                list.status!!.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun add(fav: StatusPlaylistResponse) {
        list.status!!.add(fav)
        notifyItemInserted(list.status!!.size - 1)
        notifyItemChanged(list.status!!.size - 1)
    }

    fun addAll(list: PlaylistResponseBody) {
        for (q in list.status!!) {
            add(q)
        }
    }
}