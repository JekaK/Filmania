package filmania.com.filmania2.Adapter

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.Fragments.FilmInfoFragment
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.DeleteFromFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import filmania.com.filmania2.Request.Body.Response.ResultFav
import filmania.com.filmania2.Request.Callback.DeleteFromFavouriteCallback
import filmania.com.filmania2.Request.Request.DeleteFromFavouriteRequest
import filmania.com.filmania2.ViewHolder.FavouriteViewHolder
import okhttp3.ResponseBody

/**
 * Клас - адаптер для відображення списку улюблених фільмів. Клас включає в себе логіку відображення списку фільмів,
 * а також логіку дій при натисканні кнопок.
 * **/
class FavouriteRecyclerAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var resFav: FavouriteListResponse
    private lateinit var context: FragmentActivity

    constructor(context: FragmentActivity, resFav: FavouriteListResponse) : this() {
        this.context = context
        this.resFav = resFav
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: FavouriteViewHolder
        val inflater = LayoutInflater.from(parent.context)
        viewHolder = FavouriteViewHolder(inflater.inflate(R.layout.favourite_card, parent, false))
        return viewHolder
    }

    override fun getItemCount(): Int {
        return resFav.resultNews!!.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val filmRes = resFav.resultNews!![position]
        val movieHolder = holder as FavouriteViewHolder
        movieHolder.description.text = filmRes.getMovieInfo()!!.overview
        movieHolder.title.text = filmRes.getMovieInfo()!!.originalTitle

        val requestOptions = RequestOptions()
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)

        Glide
                .with(context)
                .load(context.getString(R.string.IMAGE_BASE_URL) + filmRes.getMovieInfo()!!.posterPath)
                .apply(requestOptions)
                .into(movieHolder.poster)
        movieHolder.ratingBar.rating = (filmRes.getMovieInfo()!!.voteAverage!!.toFloat() * 5) / 10
        var selectedFragment: android.support.v4.app.Fragment? = null

        movieHolder.detailsButton.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id", filmRes.getMovieInfo()!!.id.toString())
            selectedFragment = FilmInfoFragment()
            (selectedFragment as FilmInfoFragment).arguments = bundle

            context.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frame_container, selectedFragment, selectedFragment!!::class.java.simpleName)
                    .addToBackStack(context.supportFragmentManager.findFragmentById(R.id.frame_container).javaClass.simpleName)
                    .commit()
        }
        movieHolder.delete.setOnClickListener({
            DeleteFromFavouriteRequest(context)
                    .deleteFromFavourite(DeleteFromFavouriteRequestBody(filmRes.movieId!!),
                            object : DeleteFromFavouriteCallback {
                                override fun onSuccess(resp: ResponseBody) {
                                    resFav.resultNews!!.removeAt(position)
                                    if (position != 0)
                                        notifyItemRemoved(position)
                                    else
                                        notifyDataSetChanged()
                                }

                                override fun onFail(t: Throwable) {
                                    Toasty.error(context, "Oops, something wrong.")
                                }

                            })
        })
    }

    fun clear() {
        val size = resFav.resultNews!!.size
        if (size > 0) {
            for (i in 0 until size) {
                resFav.resultNews!!.removeAt(0)
            }
            notifyItemRangeRemoved(0, size)
        }
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun add(fav: ResultFav) {
        resFav.resultNews!!.add(fav)
        notifyItemInserted(resFav.resultNews!!.size - 1)
        notifyItemChanged(resFav.resultNews!!.size - 1)
    }

    fun addAll(list: FavouriteListResponse) {
        for (q in list.resultNews!!) {
            add(q)
        }
    }
}