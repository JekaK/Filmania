package filmania.com.filmania2.Adapter

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import filmania.com.filmania2.Fragments.FilmInfoFragment
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import filmania.com.filmania2.Request.Body.Response.ResultNew
import filmania.com.filmania2.ViewHolder.FilmViewHolder
import filmania.com.filmania2.ViewHolder.PaginationVH

/**
 * Клас - адаптер для відображення списку фільмів відображених в пошуку. Клас включає в себе логіку відображення списку фільмів,
 * а також логіку дій при натисканні кнопок.
 * **/

class NewsSearchRecyclerAdapter() : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var filmResponse: NewSearchFilmListResponse
    private var isLoadingAdded = false
    private val ITEM = 0
    private val LOADING = 1
    private var isConnected: Boolean = false
    private lateinit var context: FragmentActivity

    constructor(context: FragmentActivity, filmListResponse: NewSearchFilmListResponse) : this() {
        this.context = context
        filmResponse = filmListResponse
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: RecyclerView.ViewHolder
        val inflater = LayoutInflater.from(parent.context)
        when (viewType) {
            ITEM -> viewHolder = getViewHolder(parent, inflater)
            LOADING -> {
                val v2 = inflater.inflate(R.layout.pagination_item, parent, false)
                viewHolder = PaginationVH(v2)
            }
        }

        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val result = filmResponse.results!![position]

        when (getItemViewType(position)) {
            ITEM -> {
                val productViewHolder = holder as FilmViewHolder
                productViewHolder.ratingBar.rating = (result.voteAverage!!.toFloat() * 5) / 10
                productViewHolder.description.text = result.overview
                productViewHolder.title.text = result.title
                val requestOptions = RequestOptions()
                requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL)
                var selectedFragment: android.support.v4.app.Fragment? = null

                Glide
                        .with(context)
                        .load(context.getString(R.string.IMAGE_BASE_URL) + result.posterPath)
                        .apply(requestOptions)
                        .into(productViewHolder.poster)
                productViewHolder.detailsButton.setOnClickListener {
                    val bundle = Bundle()
                    bundle.putString("id", result.id.toString())
                    selectedFragment = FilmInfoFragment()
                    (selectedFragment as FilmInfoFragment).arguments = bundle

                    context.supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frame_container, selectedFragment, selectedFragment!!::class.java.simpleName)
                            .addToBackStack(null)
                            .commit()
                }
            }
            LOADING -> {

            }
        }
    }

    private fun getViewHolder(parent: ViewGroup, inflater: LayoutInflater): FilmViewHolder {
        val viewHolder: FilmViewHolder
        val v1 = inflater.inflate(R.layout.news_card, parent, false)
        viewHolder = FilmViewHolder(v1)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return filmResponse.results!!.size
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    fun add(film: ResultNew) {
        filmResponse.results!!.add(film)
        notifyItemInserted(filmResponse.results!!.size - 1)
        notifyItemChanged(filmResponse.results!!.size - 1)
    }

    fun addAll(list: List<ResultNew>) {
        for (q in list) {
            add(q)
        }
    }

    fun setIsConnected(isConnected: Boolean) {
        this.isConnected = isConnected
    }

    fun isConnected(): Boolean {
        return isConnected
    }

    fun remove(film: ResultNew) {
        val position = filmResponse.results!!.indexOf(film)
        if (position > -1) {
            filmResponse.results!!.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun clear() {
        isLoadingAdded = false
        if (isConnected()) {
            removeLoadingFooter()
        }
        while (itemCount > 0) {
            remove(getItem(0))
        }
    }

    fun getItem(position: Int): ResultNew {
        return filmResponse.results!!.get(position)
    }

    fun isEmpty(): Boolean {
        return itemCount == 0
    }

    fun addLoadingFooter() {
        isLoadingAdded = true
        add(ResultNew())
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = filmResponse.results!!.size - 1
        if (position > -1) {
            val item = getItem(position)
            filmResponse.results!!.removeAt(position)
            notifyItemRemoved(position)

        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == filmResponse.results!!.size - 1 && isLoadingAdded) LOADING else ITEM
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}