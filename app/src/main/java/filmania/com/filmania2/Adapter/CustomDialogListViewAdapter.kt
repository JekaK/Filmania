package filmania.com.filmania2.Adapter

import android.content.Context
import android.content.res.Resources
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.github.ivbaranov.mli.MaterialLetterIcon
import es.dmoral.toasty.Toasty
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AddMovieToPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Request.AddToFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Response.AddMovieToPlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.AddToFavouriteResponse
import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.StatusPlaylistResponse
import filmania.com.filmania2.Request.Callback.AddMovieToPlaylistCallback
import filmania.com.filmania2.Request.Callback.AddToFavouriteCallback
import filmania.com.filmania2.Request.Callback.DefaultCallback
import filmania.com.filmania2.Request.Request.AddMovieToPlaylistRequest
import filmania.com.filmania2.Request.Request.AddToFavouriteRequest
import filmania.com.filmania2.ViewHolder.CustomDialogItemVH

class CustomDialogListViewAdapter : ArrayAdapter<StatusPlaylistResponse> {
    private lateinit var playlistResponse: MutableList<StatusPlaylistResponse>
    private lateinit var movieId: String
    private lateinit var defaultCallback: DefaultCallback

    constructor(context: Context,
                list: MutableList<StatusPlaylistResponse>, movieId: String,
                defaultCallback: DefaultCallback) : super(context, 0, list) {
        this.playlistResponse = list
        this.movieId = movieId
        this.defaultCallback = defaultCallback

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val item = playlistResponse[position]

        val holder = LayoutInflater.from(getContext()).inflate(R.layout.custom_dialog_item, parent, false);

        holder!!.findViewById<MaterialLetterIcon>(R.id.icon_playlist_dialog).lettersNumber = 1
        holder.findViewById<MaterialLetterIcon>(R.id.icon_playlist_dialog).letter = item.name
        holder.findViewById<TextView>(R.id.playlist_name_dialog).text = item.name
        holder.findViewById<ConstraintLayout>(R.id.item_dialog).setOnClickListener({
            if (position == 0) {
                AddToFavouriteRequest(context)
                        .addToFavouriteRequest(AddToFavouriteRequestBody(movieId),
                                object : AddToFavouriteCallback {
                                    override fun onSuccess(resp: AddToFavouriteResponse) {
                                        Toasty.success(context, "Film added to your favourite list",
                                                Toast.LENGTH_SHORT).show()
                                        defaultCallback.onSuccess()
                                    }

                                    override fun onFail(t: Throwable) {
                                        t.printStackTrace()
                                        Toasty.error(context, "Ooops,something wrong happens. Maybe this" +
                                                " film already at your favourite or something with internet connection. Who knows..."
                                                , Toast.LENGTH_SHORT).show()
                                        defaultCallback.onFail()

                                    }
                                })
            } else {
                AddMovieToPlaylistRequest(context)
                        .addMovieToPlaylist(AddMovieToPlaylistRequestBody(movieId.toInt(), item.id!!),
                                object : AddMovieToPlaylistCallback {
                                    override fun onSuccess(resp: AddMovieToPlaylistResponseBody) {
                                        Toasty.success(context, "Film added to your playlist",
                                                Toast.LENGTH_SHORT).show()
                                        defaultCallback.onSuccess()
                                    }

                                    override fun onFail(t: Throwable) {
                                        Toasty.error(context, "Ooops,something wrong happens. Maybe this" +
                                                " film already at your playlist or something with internet connection. Who knows..."
                                                , Toast.LENGTH_SHORT).show()
                                        defaultCallback.onFail()
                                    }
                                })
            }
        })
        return holder
    }
}