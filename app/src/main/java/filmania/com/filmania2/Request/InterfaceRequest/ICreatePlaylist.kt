package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.CreatePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Request.DeleteFromFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Response.CreatePlaylistResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ICreatePlaylist {
    @POST("/createPlaylist")
    fun createPlaylist(@Header("Token") token: String, @Body body: CreatePlaylistRequestBody)
            : Call<CreatePlaylistResponseBody>
}