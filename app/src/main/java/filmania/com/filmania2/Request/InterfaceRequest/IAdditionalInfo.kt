package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.AdditionalInfoRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieAdditionalInfo
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IAdditionalInfo {
    @POST("/getMovieInfo")
    fun getAdditionalInfo(@Header("Token") token: String,
                          @Body additionalInfo: AdditionalInfoRequestBody): Call<MovieAdditionalInfo>
}