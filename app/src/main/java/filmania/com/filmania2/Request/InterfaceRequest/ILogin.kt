package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.AuthRequestBody
import filmania.com.filmania2.Request.Body.Response.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface ILogin {
    @POST("/auth/login")
    fun login(@Body loginBody: AuthRequestBody): Call<LoginResponse>
}