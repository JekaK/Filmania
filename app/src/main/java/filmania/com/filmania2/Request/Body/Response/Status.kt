package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Status : Serializable {

    @SerializedName("username")
    @Expose
    var username: String? = null
    @SerializedName("playlist_c")
    @Expose
    var playlistC: Int? = null
    @SerializedName("favourite_c")
    @Expose
    var favouriteC: Int? = null
    @SerializedName("email")
    @Expose
    var email: String? = null
    @SerializedName("photo")
    @Expose
    var photo: String? = null

}