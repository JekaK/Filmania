package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Callback.LogoutCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.ILogout
import filmania.com.filmania2.Util.LocalInfoSave
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LogoutRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun logout(callback: LogoutCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val logout = retrofit.create(ILogout::class.java)
        val call = logout.logout(LocalInfoSave(context).getAccessToken()!!)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)

            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response!!.errorBody() == null)
                    callback.onSuccess(response.body()!!)
                else
                    callback.onFail(Throwable(response.errorBody().toString()))
            }

        })
    }
}