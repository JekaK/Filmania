package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.AddMovieToPlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse

interface AddMovieToPlaylistCallback {
    public fun onSuccess(resp: AddMovieToPlaylistResponseBody)
    public fun onFail(t: Throwable)
}