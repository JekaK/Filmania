package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AdditionalInfoRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieAdditionalInfo
import filmania.com.filmania2.Request.Callback.AdditionalInfoCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IAdditionalInfo
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AdditionalInfoRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun getAdditionalInfo(body: AdditionalInfoRequestBody, callback: AdditionalInfoCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val additionalInfo = retrofit.create(IAdditionalInfo::class.java)
        val call = additionalInfo.getAdditionalInfo(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<MovieAdditionalInfo> {
            override fun onFailure(call: Call<MovieAdditionalInfo>?, t: Throwable?) {
                if (t != null) {
                    callback.onFail(t)
                }
            }

            override fun onResponse(call: Call<MovieAdditionalInfo>?, response: Response<MovieAdditionalInfo>?) {
                if (response != null) {
                    if (response.errorBody() != null) {
                        callback.onFail(Throwable(response.errorBody().toString()))
                    } else {
                        callback.onSuccess(response.body()!!)
                    }
                }
            }
        })
    }
}