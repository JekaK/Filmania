package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.DeleteMovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Request.DeletePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody
import filmania.com.filmania2.Request.Body.Response.DeleteResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IDeletePlaylist {
    @POST("/deletePlaylist")
    fun deletePlaylist(@Header("Token") token: String,
                       @Body body: DeletePlaylistRequestBody)
            : Call<DeleteMovieFromPlaylistResponseBody>
}