package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.ChangeNameRequestBody
import filmania.com.filmania2.Request.Body.Response.ChangeNameResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IEditNamePlaylist {
    @POST("/editPlaylistName")
    fun editName(@Header("Token") token: String, @Body editName: ChangeNameRequestBody): Call<ChangeNameResponseBody>
}