package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StatusLogin : Serializable {

    @SerializedName("key")
    @Expose
    var key: String? = null

}