package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface IGetPlaylists {
    @POST("/getPlaylists")
    fun getPlaylists(@Header("Token") token: String): Call<PlaylistResponseBody>
}