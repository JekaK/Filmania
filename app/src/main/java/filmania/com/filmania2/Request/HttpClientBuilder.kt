package filmania.com.filmania2.Request

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


class HttpClientBuilder private constructor() {
    private var client: OkHttpClient? = null

    val okHttpClient: OkHttpClient?
        get() {
            if (client == null) {
                initOkHttpClient()
            }
            return client
        }

    fun initOkHttpClient() {
        client = OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS)
                .writeTimeout(100, TimeUnit.SECONDS)
                .build()
    }

    companion object {
        private var httpClientBuilder: HttpClientBuilder? = null

        val instance: HttpClientBuilder
            get() {
                if (httpClientBuilder == null) {
                    synchronized(HttpClientBuilder::class.java) {
                        httpClientBuilder = HttpClientBuilder()
                    }
                }
                return this.httpClientBuilder!!
            }
    }
}