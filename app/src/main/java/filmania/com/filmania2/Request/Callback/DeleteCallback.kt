package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody

interface DeleteCallback {
    public fun onSuccess(resp: DeleteMovieFromPlaylistResponseBody)
    public fun onFail(t: Throwable)
}