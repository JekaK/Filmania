package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.SearchResponse

interface SearchCallback {
    public fun onSuccess(resp: SearchResponse)
    public fun onFail(t: Throwable)
}