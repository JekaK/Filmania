package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Response.UserProfileResponseBody
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface IUserInfoResponse {
    @POST("/user")
    fun getUserInfo(@Header("Token") token: String): Call<UserProfileResponseBody>
}