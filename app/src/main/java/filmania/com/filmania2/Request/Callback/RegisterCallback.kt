package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.RegisterResponse

interface RegisterCallback {
    public fun onSuccess(resp: RegisterResponse)
    public fun onFail(t: Throwable)
}