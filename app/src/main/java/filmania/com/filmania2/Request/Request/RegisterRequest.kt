package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AuthRequestBody
import filmania.com.filmania2.Request.Body.Response.RegisterResponse
import filmania.com.filmania2.Request.Callback.RegisterCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IRegister
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RegisterRequest(context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    public fun registerUser(body: AuthRequestBody, callback: RegisterCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val register = retrofit.create(IRegister::class.java)
        val call = register.register(body)
        call.enqueue(object : Callback<RegisterResponse> {
            override fun onFailure(call: Call<RegisterResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<RegisterResponse>?, response: Response<RegisterResponse>?) {
                if (response!!.errorBody() == null)
                    callback.onSuccess(response.body()!!)
                else
                    callback.onFail(Throwable(response.errorBody().toString()))
            }
        })
    }

}