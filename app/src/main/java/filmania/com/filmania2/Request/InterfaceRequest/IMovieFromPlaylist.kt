package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.MovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieFromPlaylistResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IMovieFromPlaylist {
    @POST("/getPlaylistMovies")
    fun getPlaylistMovies(@Header("Token") token: String,
                          @Body body: MovieFromPlaylistRequestBody): Call<MovieFromPlaylistResponse>
}