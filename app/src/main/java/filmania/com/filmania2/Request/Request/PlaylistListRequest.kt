package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody
import filmania.com.filmania2.Request.Callback.PlaylistsListCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IGetPlaylists
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PlaylistListRequest(private val context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun getPlaylistList(callback: PlaylistsListCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val playlistList = retrofit.create(IGetPlaylists::class.java)
        val call = playlistList.getPlaylists(LocalInfoSave(context).getAccessToken()!!)
        call.enqueue(object : Callback<PlaylistResponseBody> {
            override fun onFailure(call: Call<PlaylistResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<PlaylistResponseBody>?, responseResponseBody: Response<PlaylistResponseBody>?) {
                if (responseResponseBody!!.errorBody() !== null) {
                    callback.onFail(Throwable(responseResponseBody!!.errorBody().toString()))
                } else {
                    callback.onSuccess(responseResponseBody!!.body()!!)
                }
            }
        })
    }
}