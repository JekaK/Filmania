package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody

interface DeleteMovieFromPlaylistCallback {
    public fun onSuccess(resp: DeleteMovieFromPlaylistResponseBody)
    public fun onFail(t: Throwable)
}