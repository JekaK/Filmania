package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddToFavouriteResponse {

    @SerializedName("id")
    @Expose
    var id: Int? = null

}