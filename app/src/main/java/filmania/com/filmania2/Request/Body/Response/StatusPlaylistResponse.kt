package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StatusPlaylistResponse {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("user_key")
    @Expose
    var userKey: String? = null

    constructor(name: String?) {
        this.name = name
    }
}