package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.AuthRequestBody
import filmania.com.filmania2.Request.Body.Response.RegisterResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface IRegister {
    @POST("/auth/register")
    fun register(@Body loginBody: AuthRequestBody): Call<RegisterResponse>
}