package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AddMovieToPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.AddMovieToPlaylistResponseBody
import filmania.com.filmania2.Request.Callback.AddMovieToPlaylistCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IAddMovieToPlaylist
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AddMovieToPlaylistRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun addMovieToPlaylist(body: AddMovieToPlaylistRequestBody, callback: AddMovieToPlaylistCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val add = retrofit.create(IAddMovieToPlaylist::class.java)
        val call = add.addMovieToPlaylist(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<AddMovieToPlaylistResponseBody> {
            override fun onFailure(call: Call<AddMovieToPlaylistResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<AddMovieToPlaylistResponseBody>?, response: Response<AddMovieToPlaylistResponseBody>?) {
                if (response!!.errorBody() === null) {
                    callback.onSuccess(response!!.body()!!)
                } else {
                    callback.onFail(Throwable(response!!.errorBody().toString()))
                }
            }
        })
    }
}