package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AuthRequestBody
import filmania.com.filmania2.Request.Body.Response.LoginResponse
import filmania.com.filmania2.Request.Callback.LoginCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.ILogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LoginRequest(context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)
    private var c = context

    public fun loginUser(body: AuthRequestBody, callback: LoginCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val login = retrofit.create(ILogin::class.java)
        val call = login.login(body)
        call.enqueue(object : Callback<LoginResponse> {
            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                if (response!!.errorBody() == null)
                    callback.onSuccess(response.body()!!)
                else
                    callback.onFail(Throwable(response.errorBody().toString()))
            }
        })
    }
}