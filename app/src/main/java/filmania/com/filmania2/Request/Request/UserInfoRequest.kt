package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.UserProfileResponseBody
import filmania.com.filmania2.Request.Callback.UserInfoCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IUserInfoResponse
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserInfoRequest(var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun getUserInfo(callback: UserInfoCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val userInfo = retrofit.create(IUserInfoResponse::class.java)
        val call = userInfo.getUserInfo(LocalInfoSave(context).getAccessToken()!!)
        call.enqueue(object : Callback<UserProfileResponseBody> {
            override fun onFailure(call: Call<UserProfileResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<UserProfileResponseBody>?, response: Response<UserProfileResponseBody>?) {
                if (response!!.errorBody() === null) {
                    callback.onSuccess(response!!.body()!!)
                } else {
                    callback.onFail(Throwable(response!!.errorBody().toString()))
                }
            }
        })
    }
}