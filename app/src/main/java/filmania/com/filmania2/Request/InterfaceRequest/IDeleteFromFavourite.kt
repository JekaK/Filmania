package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.DeleteFromFavouriteRequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IDeleteFromFavourite {
    @POST("/removeFromFavourite")
public fun removeFromFavourite(@Header("Token") token: String,@Body del:DeleteFromFavouriteRequestBody):Call<ResponseBody>
}