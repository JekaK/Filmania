package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.ChangeNameRequestBody
import filmania.com.filmania2.Request.Body.Response.ChangeNameResponseBody
import filmania.com.filmania2.Request.Callback.ChangeNameCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IEditNamePlaylist
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class EditPlaylistRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun changeName(body: ChangeNameRequestBody, callback: ChangeNameCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val changeName = retrofit.create(IEditNamePlaylist::class.java)
        val call = changeName.editName(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<ChangeNameResponseBody> {
            override fun onResponse(call: Call<ChangeNameResponseBody>?, response: Response<ChangeNameResponseBody>?) {
                if (response!!.errorBody() != null) {
                    callback.onFail(Throwable(response.errorBody().toString()))
                } else {
                    callback.onSuccess(response.body()!!)
                }
            }

            override fun onFailure(call: Call<ChangeNameResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }
        })
    }
}