package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.AddToFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Response.AddToFavouriteResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IAddToFavourite {
    @POST("/addToFavourite")
    fun addToFavourite(@Header("Token") token: String,
                       @Body addToFavouriteRequestBody: AddToFavouriteRequestBody):
            Call<AddToFavouriteResponse>
}