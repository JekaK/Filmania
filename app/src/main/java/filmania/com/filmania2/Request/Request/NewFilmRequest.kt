package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.NewFilmsRequestBody
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import filmania.com.filmania2.Request.Callback.NewSearchFilmsCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.INewFilms
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewFilmRequest(context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)
    fun getNewFilms(token: String, page: Int, callback: NewSearchFilmsCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val request = retrofit.create(INewFilms::class.java)
        val call = request.getNewFilms(token, NewFilmsRequestBody(page))
        call.enqueue(object : Callback<NewSearchFilmListResponse> {
            override fun onFailure(call: Call<NewSearchFilmListResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<NewSearchFilmListResponse>?, response: Response<NewSearchFilmListResponse>?) {
                if (response!!.errorBody() == null) {
                    callback.onSuccess(response.body()!!)
                } else {
                    callback.onFail(Throwable(response.errorBody().toString()))
                }
            }
        })
    }
}