package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.CreatePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.CreatePlaylistResponseBody
import filmania.com.filmania2.Request.Callback.CreatePlaylistCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.ICreatePlaylist
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CreatePlaylistRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun createPlaylist(body: CreatePlaylistRequestBody, callback: CreatePlaylistCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val createPlaylist = retrofit.create(ICreatePlaylist::class.java)
        val call = createPlaylist.createPlaylist(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<CreatePlaylistResponseBody> {
            override fun onFailure(call: Call<CreatePlaylistResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<CreatePlaylistResponseBody>?, response: Response<CreatePlaylistResponseBody>?) {
                if (response!!.errorBody() === null) {
                    callback.onSuccess(response!!.body()!!)
                } else {
                    callback.onFail(Throwable(response!!.errorBody().toString()))
                }
            }
        })
    }
}