package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StatusMoviesFromPlaylist {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("playlist_id")
    @Expose
    var playlistId: Int? = null
    @SerializedName("movie_info")
    @Expose
    var movieInfo: MovieInfo? = null
    @SerializedName("movie_id")
    @Expose
    var movieId: String? = null

}