package filmania.com.filmania2.Request.Callback

interface DefaultCallback {
    fun onSuccess();
    fun onFail();
}