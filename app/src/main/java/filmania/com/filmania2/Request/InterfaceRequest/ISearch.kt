package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.SearchRequestBody
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface ISearch {
    @POST("/search")
    fun search(@Header("Token") token: String, @Body body: SearchRequestBody): Call<NewSearchFilmListResponse>
}