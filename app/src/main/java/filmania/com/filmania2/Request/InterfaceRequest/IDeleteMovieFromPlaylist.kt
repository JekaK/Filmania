package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.DeleteMovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IDeleteMovieFromPlaylist {
    @POST("/deleteFromPlaylist")
    fun deleteMovieFromPlaylist(@Header("Token") token: String, @Body body: DeleteMovieFromPlaylistRequestBody)
            : Call<DeleteMovieFromPlaylistResponseBody>
}