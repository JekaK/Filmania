package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.AddToFavouriteResponse

interface AddToFavouriteCallback {
    public fun onSuccess(resp: AddToFavouriteResponse)
    public fun onFail(t: Throwable)
}