package filmania.com.filmania2.Request.InterfaceRequest

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface ILogout {
    @POST("/logout")
    fun logout(@Header("Token") token: String): Call<ResponseBody>
}