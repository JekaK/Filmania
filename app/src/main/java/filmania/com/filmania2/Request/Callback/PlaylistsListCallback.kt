package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.PlaylistResponseBody

interface PlaylistsListCallback {
    public fun onSuccess(resp: PlaylistResponseBody)
    public fun onFail(t: Throwable)
}