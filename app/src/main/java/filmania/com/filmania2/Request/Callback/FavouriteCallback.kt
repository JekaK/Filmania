package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse

interface FavouriteCallback {
    public fun onSuccess(resp: FavouriteListResponse)
    public fun onFail(t: Throwable)
}