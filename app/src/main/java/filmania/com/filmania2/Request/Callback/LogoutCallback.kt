package filmania.com.filmania2.Request.Callback

import okhttp3.ResponseBody

interface LogoutCallback {
    public fun onSuccess(resp: ResponseBody)
    public fun onFail(t: Throwable)
}