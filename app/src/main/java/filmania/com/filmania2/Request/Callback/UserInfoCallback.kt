package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.UserProfileResponseBody

interface UserInfoCallback {
    public fun onSuccess(resp: UserProfileResponseBody)
    public fun onFail(t: Throwable)
}