package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.NewFilmsRequestBody
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface INewFilms {
    @POST("/newFilms")
    fun getNewFilms(@Header("Token") token: String, @Body newFilmsRequestBody: NewFilmsRequestBody): Call<NewSearchFilmListResponse>
}