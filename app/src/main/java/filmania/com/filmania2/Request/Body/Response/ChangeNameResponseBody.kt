package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ChangeNameResponseBody {

    @SerializedName("status")
    @Expose
    var status: String? = null

}