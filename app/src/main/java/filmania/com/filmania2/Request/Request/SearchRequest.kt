package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.SearchRequestBody
import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse
import filmania.com.filmania2.Request.Body.Response.SearchResponse
import filmania.com.filmania2.Request.Callback.NewSearchFilmsCallback
import filmania.com.filmania2.Request.Callback.SearchCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.ISearch
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SearchRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun search(query: String, page: Int, callback: NewSearchFilmsCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val search = retrofit.create(ISearch::class.java)
        val call = search.search(LocalInfoSave(context).getAccessToken()!!,
                SearchRequestBody(query, page))
        call.enqueue(object : Callback<NewSearchFilmListResponse> {
            override fun onFailure(call: Call<NewSearchFilmListResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<NewSearchFilmListResponse>?, response: Response<NewSearchFilmListResponse>?) {
                if (response!!.errorBody() !== null) {
                    callback.onFail(Throwable(response!!.errorBody().toString()))
                } else {
                    callback.onSuccess(response!!.body()!!)
                }
            }
        })
    }
}