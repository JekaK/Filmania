package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BelongsToCollection:Serializable{

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("poster_path")
    @Expose
    var posterPath: Any? = null
    @SerializedName("backdrop_path")
    @Expose
    var backdropPath: Any? = null

}
