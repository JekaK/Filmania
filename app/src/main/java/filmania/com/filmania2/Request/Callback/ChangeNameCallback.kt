package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.ChangeNameResponseBody

interface ChangeNameCallback {
    public fun onSuccess(resp: ChangeNameResponseBody)
    public fun onFail(t: Throwable)
}