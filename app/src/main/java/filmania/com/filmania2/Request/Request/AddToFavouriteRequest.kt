package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.AddToFavouriteRequestBody
import filmania.com.filmania2.Request.Body.Response.AddToFavouriteResponse
import filmania.com.filmania2.Request.Callback.AddToFavouriteCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IAddToFavourite
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AddToFavouriteRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun addToFavouriteRequest(addToFavouriteRequestBody: AddToFavouriteRequestBody,
                              callback: AddToFavouriteCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val addToFavourite = retrofit.create(IAddToFavourite::class.java)
        val call = addToFavourite.addToFavourite(LocalInfoSave(context).getAccessToken()!!,
                addToFavouriteRequestBody)
        call.enqueue(object : Callback<AddToFavouriteResponse> {
            override fun onFailure(call: Call<AddToFavouriteResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<AddToFavouriteResponse>?, response: Response<AddToFavouriteResponse>?) {
                if (response!!.errorBody() === null) {
                    callback.onSuccess(response!!.body()!!)
                } else {
                    callback.onFail(Throwable(response!!.errorBody().toString()))
                }
            }
        })
    }

}