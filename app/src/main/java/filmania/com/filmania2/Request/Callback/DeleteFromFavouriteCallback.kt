package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import okhttp3.ResponseBody

interface DeleteFromFavouriteCallback {
    public fun onSuccess(resp: ResponseBody)
    public fun onFail(t: Throwable)
}