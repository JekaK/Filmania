package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.CreatePlaylistResponseBody

interface CreatePlaylistCallback {
    public fun onSuccess(resp: CreatePlaylistResponseBody)
    public fun onFail(t: Throwable)
}