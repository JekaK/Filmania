package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class StatusRegister : Serializable {

    @SerializedName("success")
    @Expose
    private var success: Boolean? = null
    @SerializedName("token")
    @Expose
    private var token: String? = null

    fun getSuccess(): Boolean? {
        return success
    }

    fun setSuccess(success: Boolean?) {
        this.success = success
    }

    fun getToken(): String? {
        return token
    }

    fun setToken(token: String) {
        this.token = token
    }
}