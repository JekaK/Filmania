package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Request.AddMovieToPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.AddMovieToPlaylistResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface IAddMovieToPlaylist {
    @POST("/addToPlaylist")
    fun addMovieToPlaylist(@Header("Token") token: String, @Body body: AddMovieToPlaylistRequestBody)
            : Call<AddMovieToPlaylistResponseBody>

}