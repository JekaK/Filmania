package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.MovieFromPlaylistResponse

interface MovieFromPlaylistCallback {
    public fun onSuccess(resp: MovieFromPlaylistResponse)
    public fun onFail(t: Throwable)
}