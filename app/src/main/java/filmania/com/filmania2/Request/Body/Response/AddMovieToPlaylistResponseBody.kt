package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AddMovieToPlaylistResponseBody {
    @SerializedName("status")
    @Expose
    private var status: String? = null

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String) {
        this.status = status
    }
}