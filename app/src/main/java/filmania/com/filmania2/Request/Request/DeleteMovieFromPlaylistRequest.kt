package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.DeleteMovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Request.DeletePlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.DeleteMovieFromPlaylistResponseBody
import filmania.com.filmania2.Request.Callback.DeleteMovieFromPlaylistCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.ICreatePlaylist
import filmania.com.filmania2.Request.InterfaceRequest.IDeleteMovieFromPlaylist
import filmania.com.filmania2.Request.InterfaceRequest.IDeletePlaylist
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DeleteMovieFromPlaylistRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun deleteMovieFromPlaylist(body: DeleteMovieFromPlaylistRequestBody,
                                callback: DeleteMovieFromPlaylistCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val delete = retrofit.create(IDeleteMovieFromPlaylist::class.java)
        val call = delete.deleteMovieFromPlaylist(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<DeleteMovieFromPlaylistResponseBody> {
            override fun onFailure(call: Call<DeleteMovieFromPlaylistResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<DeleteMovieFromPlaylistResponseBody>?,
                                    response: Response<DeleteMovieFromPlaylistResponseBody>?) {
                if (response!!.errorBody() != null) {
                    callback.onFail(Throwable(response.errorBody().toString()))
                } else {
                    callback.onSuccess(response.body()!!)
                }
            }
        })
    }
}