package filmania.com.filmania2.Request.InterfaceRequest

import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST

interface IFavourite {
    @POST("/getFavourite")
    fun getFavourite(@Header("Token") token: String): Call<FavouriteListResponse>
}