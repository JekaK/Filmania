package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Response.FavouriteListResponse
import filmania.com.filmania2.Request.Callback.FavouriteCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IFavourite
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FavouriteRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun getFavourite(callback: FavouriteCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val favourite = retrofit.create(IFavourite::class.java)
        val call = favourite.getFavourite(LocalInfoSave(context).getAccessToken()!!)
        call.enqueue(object : Callback<FavouriteListResponse> {
            override fun onFailure(call: Call<FavouriteListResponse>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<FavouriteListResponse>?, response: Response<FavouriteListResponse>?) {
                if (response!!.errorBody() != null) {
                    callback.onFail(Throwable(response.errorBody().toString()))
                } else {
                    callback.onSuccess(response.body()!!)
                }
            }
        })
    }
}