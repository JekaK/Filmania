package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.MovieFromPlaylistRequestBody
import filmania.com.filmania2.Request.Body.Response.MovieFromPlaylistResponse
import filmania.com.filmania2.Request.Callback.MovieFromPlaylistCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IMovieFromPlaylist
import filmania.com.filmania2.Util.LocalInfoSave
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MoviesFromPlaylistRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun getMoviesFromPlaylist(body: MovieFromPlaylistRequestBody, callback: MovieFromPlaylistCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val request = retrofit.create(IMovieFromPlaylist::class.java)
        val call = request.getPlaylistMovies(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object :Callback<MovieFromPlaylistResponse>{
            override fun onFailure(call: Call<MovieFromPlaylistResponse>?, t: Throwable?) {
                callback.onFail(t!!)

            }

            override fun onResponse(call: Call<MovieFromPlaylistResponse>?, response: Response<MovieFromPlaylistResponse>?) {
                if (response!!.errorBody() == null)
                    callback.onSuccess(response.body()!!)
                else
                    callback.onFail(Throwable(response.errorBody().toString()))
            }
        })
    }
}