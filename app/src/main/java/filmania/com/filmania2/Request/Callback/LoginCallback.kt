package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.LoginResponse

interface LoginCallback {
    public fun onSuccess(resp: LoginResponse)
    public fun onFail(t: Throwable)
}