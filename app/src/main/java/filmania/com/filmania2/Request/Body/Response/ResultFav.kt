package filmania.com.filmania2.Request.Body.Response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ResultFav : Serializable {

    @SerializedName("id")
    @Expose
    var id: Int? = null
    @SerializedName("user_token")
    @Expose
    var userToken: String? = null
    @SerializedName("movie_info")
    @Expose
    private var movieInfo: MovieInfo? = null
    @SerializedName("movie_id")
    @Expose
    var movieId: String? = null

    fun getMovieInfo(): MovieInfo? {
        return movieInfo
    }

    fun setMovieInfo(movieInfo: MovieInfo) {
        this.movieInfo = movieInfo
    }

}