package filmania.com.filmania2.Request.Request

import android.content.Context
import filmania.com.filmania2.R
import filmania.com.filmania2.Request.Body.Request.DeleteFromFavouriteRequestBody
import filmania.com.filmania2.Request.Callback.DeleteFromFavouriteCallback
import filmania.com.filmania2.Request.HttpClientBuilder
import filmania.com.filmania2.Request.InterfaceRequest.IDeleteFromFavourite
import filmania.com.filmania2.Util.LocalInfoSave
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DeleteFromFavouriteRequest(private var context: Context) {
    private var baseUrl: String = context.resources.getString(R.string.BASE_URL)

    fun deleteFromFavourite(body: DeleteFromFavouriteRequestBody, callback: DeleteFromFavouriteCallback) {
        val builder = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(HttpClientBuilder.instance.okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
        val retrofit = builder.build()
        val deleteFromFavourite = retrofit.create(IDeleteFromFavourite::class.java)
        val call = deleteFromFavourite.removeFromFavourite(LocalInfoSave(context).getAccessToken()!!,
                body)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                callback.onFail(t!!)
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response!!.errorBody() != null) {
                    callback.onFail(Throwable(response.errorBody().toString()))
                } else {
                    callback.onSuccess(response.body()!!)
                }
            }
        })
    }
}