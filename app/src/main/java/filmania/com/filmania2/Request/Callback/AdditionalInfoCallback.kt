package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.MovieAdditionalInfo

interface AdditionalInfoCallback {
    public fun onSuccess(resp: MovieAdditionalInfo)
    public fun onFail(t: Throwable)
}