package filmania.com.filmania2.Request.Callback

import filmania.com.filmania2.Request.Body.Response.NewSearchFilmListResponse

interface NewSearchFilmsCallback {
    public fun onSuccess(resp: NewSearchFilmListResponse)
    public fun onFail(t: Throwable)
}